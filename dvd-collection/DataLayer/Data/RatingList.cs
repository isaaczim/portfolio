﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Data
{
    public static class RatingList
    {
        public static IEnumerable<Ratings> GetAllRatings()
        {
            IEnumerable<Ratings> Ratings = new List<Ratings>()
            {
                new Ratings()
                {
                    RatingID = 1,
                    Name = "G"
                },
                new Ratings()
                {
                    RatingID = 2,
                    Name = "PG"
                },
                new Ratings()
                {
                    RatingID = 3,
                    Name = "PG-13"
                },
                new Ratings()
                {
                    RatingID = 4,
                    Name = "R"
                },
                new Ratings()
                {
                    RatingID = 4,
                    Name = "NC-17"
                }
            };
            return Ratings;
        }
    }
}
