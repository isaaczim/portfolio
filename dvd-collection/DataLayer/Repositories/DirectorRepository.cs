﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DataLayer.Config;
using DataLayer.Data;

namespace DataLayer.Repositories
{
    public class DirectorRepository
    {
        public List<Director> GetAllDirectors()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var directors = cn.Query<Director>("Select * from directors");
                return directors.ToList();
            }
        }

        public void AddDirector(Director director)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                cn.Execute("Insert directors(name) values(@Name)", new {@Name = director.Name});
            }
        }

        public void AddDvd(int directorId, int dvdId)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("dvdId", dvdId);
                p.Add("DirectorId", directorId);
                cn.Execute("Insert directorjunction(dvdid, directorid) values(@dvdId, @DirectorId)",
                    p);
            }
        }

        public Director GetDirector(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var director = cn.Query<Director>(
                    "Select name from directors d inner join directorjunction dj on dj.DirectorID = d.directorid inner join dvds dv on dv.dvdid = dj.dvdid where dv.dvdid = @id", new {@id = id}).FirstOrDefault();
                return director;
            }
        }
    }
}
