﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer.Data;
using DataLayer.Repositories;
using DataLayer.ViewModel;

namespace DVDCollection.Controllers
{
    public class HomeController : Controller
    {
        DvdRepository _repo = new DvdRepository();
        ActorsRepository _arepo = new ActorsRepository();
        DirectorRepository _drepo = new DirectorRepository();
        StudioRepositroy _srepo = new StudioRepositroy();
        // GET: Home
        public ActionResult Index()
        {
            var Dvds = _repo.GetAll().OrderBy(m=>m.Title).ToList();
            return View(Dvds);
        }

        public ActionResult ViewTable()
        {
            return View();
        }

        public ActionResult AddDvd()
        {
            DvdVM dvdVM = new DvdVM();
            return View(dvdVM);
        }

        [HttpPost]
        public ActionResult AddDvd(DvdVM dvdVm)
        {
            if (ModelState.IsValid)
            {
                _repo.AddDvd(dvdVm.Dvd);
                dvdVm.Dvd.DvdID = _repo.GetDvdId(dvdVm.Dvd.Title);
                _drepo.AddDvd(dvdVm.Director.DirectorID, dvdVm.Dvd.DvdID);
                _srepo.AddDvd(dvdVm.Studio.StudioID, dvdVm.Dvd.DvdID);
                //_arepo.AddDvd(dvdVm.Actor.ActorID, dvdVm.Dvd.DvdID);

                return RedirectToAction("Index");
            }
            return View(dvdVm);
        }

        public ActionResult ViewDvd(int id)
        {
            var DvdVM = new DvdVM();
            DvdVM.Dvd = _repo.GetDvd(id);
            DvdVM.Studio = _srepo.GetStudio(DvdVM.Dvd.DvdID);
            DvdVM.Director = _drepo.GetDirector(DvdVM.Dvd.DvdID);
            DvdVM.Dvd.Actors = _arepo.GetActorsDvd(DvdVM.Dvd.DvdID);
            return View(DvdVM);
        }

        public ActionResult AddDirector()
        {
            var director = new Director();
            return View(director);
        }

        [HttpPost]
        public ActionResult AddDirector(Director director)
        {
            if (ModelState.IsValid)
            {
                _drepo.AddDirector(director);
                return RedirectToAction("AddDirector");
            }
            return View(director);
        }

        public ActionResult AddStudio()
        {
            var studio = new Studio();
            return View(studio);
        }

        [HttpPost]
        public ActionResult AddStudio(Studio studio)
        {
            if (ModelState.IsValid)
            {
                _srepo.AddStudio(studio);
                return RedirectToAction("AddStudio");
            }
            return View(studio);
        }

        public ActionResult AddActorToDvd(int id)
        {
            DvdVM dvdVm = new DvdVM();
            dvdVm.Dvd = _repo.GetDvd(id);
            dvdVm.ActorsList = _arepo.GetAllActors();
            dvdVm.Dvd.Actors = _repo.GetActors(id);
            dvdVm.Actor = new Actor();
            return View(dvdVm);
        }

        [HttpPost]
        public ActionResult AddActorToDvd(DvdVM model)
        {
            if (ModelState.IsValid)
            {
                if (model.Dvd != null)
                {
                    _arepo.AddDvd(model.Actor.ActorID, model.Dvd.DvdID);
                }
                else
                {
                    _arepo.AddActor(model.Actor);
                }

                return RedirectToAction("AddActorToDvd");
            }
            return View(model);
        }
    }
}