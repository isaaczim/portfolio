﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataLayer.Data;
using DataLayer.Repositories;

namespace DVDCollection.Controllers
{
    public class DirectorController : ApiController
    {
        DirectorRepository _repo = new DirectorRepository();

        public List<Director> Get()
        {
            return _repo.GetAllDirectors();
        }

        public Director Get(int id)
        {
            return _repo.GetDirector(id);
        }
    }
}