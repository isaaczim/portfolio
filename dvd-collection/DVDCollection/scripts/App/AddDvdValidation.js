﻿$(document)
    .ready(function () {
        $('#DvdForm')
            .validate({
                rules: {
                    "Dvd.Title": {
                        required: true
                    },
                    "Dvd.ReleaseDate": {
                        required: true,
                        date: true
                    },
                    "Dvd.Details": {
                        required: true
                    },
                    "Dvd.ImageLink": {
                        required: true
                    },
                    "Dvd.RatingID": {
                        required: true
                    },
                    "Director.DirectorID": {
                        required: true
                    },
                    "Studio.StudioID": {
                        required: true
                    }
                },
                messages:{
                    "Dvd.Title": "Please enter the title",
                    "Dvd.ReleaseDate": "Please select a release date",
                    "Dvd.Details": "Please enter a short description of the movie",
                    "Dvd.ImageLink": "Please enter a link to an image for the cover",
                    "Dvd.RatingID": "Please select a Rating",
                    "Director.DirectorID": "Please select a Director. You can also add a new one by clicking the button below.",
                    "Studio.StudioID": "Please select a Studio. You can also add a new one by clicking the button below."
            }
                
            });
    });