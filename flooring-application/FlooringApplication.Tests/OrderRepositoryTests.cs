﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;
using FlooringApplication.Data;
using FlooringApplication.Models;
using NUnit.Framework;

namespace FlooringApplication.Tests
{
    [TestFixture]
    public class OrderRepositoryTests
    {
        [Test]
        public void FoundAccountReturnsSuccess()
        {
            var _repo = new OrderRepository();
            var order = _repo.GetOrder(1, new List<Order>()
            {
                new Order(){
                OrderNumber = 1,
                CustomerName = "John",
                Area = 21,
                DateTime = DateTime.Parse("11/21/2000"),
                Product = new Product() { CostPerSf = 20, LaborCostPerSf = 15, Type = "Wood" },
                State = new State() { StateName = "Kansas", Abbreviation = "KS", TaxRate = 0.09m },
                LaborCost = 315,
                MaterialCost = 420,
                TaxCost = 66.15m,
                Total = 801.15m
                }
            });

            Assert.AreEqual("John", order.CustomerName);
            Assert.AreEqual("Wood", order.Product.Type);
        }

        [Test]
        public void NotFoundAccountReturnsFail()
        {
            var _repo = new OrderRepository();
            var response = _repo.CheckDate(new DateTime(10 / 21 / 2000));
            Assert.IsFalse(response.Success);
        }

        [Test]
        public void AddOrderSuccessfull()
        {
            var _repo = new OrderRepository();
            var order = new Order()
            {
                OrderNumber = 1,
                CustomerName = "John",
                Area = 21,
                DateTime = DateTime.Parse("11/21/2000"),
                Product = new Product() { CostPerSf = 20, LaborCostPerSf = 15, Type = "Wood" },
                State = new State() { StateName = "Kansas", Abbreviation = "KS", TaxRate = 0.09m },
                LaborCost = 315,
                MaterialCost = 420,
                TaxCost = 66.15m,
                Total = 801.15m
            };
            _repo.CreateDate(order.DateTime);
            _repo.AddOrder(order);

            var orders = _repo.GetAllOrderPerDate(order.DateTime).Count;
            Assert.AreEqual(1, orders);
        }

        [Test]
        public void RemoveOrderSuccessfull()
        {
            var _repo = new OrderRepository();
            var order = new Order()
            {
                OrderNumber = 1,
                CustomerName = "John",
                Area = 21,
                DateTime = DateTime.Parse("11/21/2000"),
                Product = new Product() { CostPerSf = 20, LaborCostPerSf = 15, Type = "Wood" },
                State = new State() { StateName = "Kansas", Abbreviation = "KS", TaxRate = 0.09m },
                LaborCost = 315,
                MaterialCost = 420,
                TaxCost = 66.15m,
                Total = 801.15m
            };
            _repo.RemoveOrder(order);
            var orderLength = _repo.GetAllOrderPerDate(order.DateTime).Count;

            Assert.AreEqual(1, orderLength);
        }
    }
}
