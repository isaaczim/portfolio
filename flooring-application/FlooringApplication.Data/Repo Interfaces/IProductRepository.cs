﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Models;

namespace FlooringApplication.Data.Repo_Interfaces
{
    public interface IProductRepository
    {
        List<Product> GetAllProducts();
    }
}
