﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data.Repo_Interfaces;
using FlooringApplication.Models;

namespace FlooringApplication.Data
{
    public class FakeProductRepository : IProductRepository
    {
        private static List<Product> AllProducts;

        public FakeProductRepository()
        {
            AllProducts = new List<Product>();

            if (AllProducts.Count == 0)
            {
                AllProducts = new List<Product>()
                {
                    new Product() {CostPerSf = 20, LaborCostPerSf = 15, Type = "wood"},
                    new Product() {CostPerSf = 30, LaborCostPerSf = 25, Type = "Stone"},
                };
            }
        }

        public List<Product> GetAllProducts()
        {
            return AllProducts;
        }
    }
}
