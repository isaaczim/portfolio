﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Models;

namespace FlooringApplication.Data
{
    public class FakeOrderRepository : IOrderRepository
    {
        private static Dictionary<string, List<Order>> TestFiles;

        public FakeOrderRepository()
        {
            TestFiles = new Dictionary<string, List<Order>>();

            if (TestFiles.Count == 0)
            {
                TestFiles = new Dictionary<string, List<Order>>();


                TestFiles.Add("Orders_11212000.txt", new List<Order>
                {
                    new Order()
                    {
                        OrderNumber = 1,
                        CustomerName = "John",
                        Area = 21,
                        DateTime = DateTime.Parse("11/21/2000"),
                        Product = new Product() {CostPerSf = 20, LaborCostPerSf = 15, Type = "Wood"},
                        State = new State() {StateName = "Kansas", Abbreviation = "KS", TaxRate = 0.09m},
                        LaborCost = 315,
                        MaterialCost = 420,
                        TaxCost = 66.15m,
                        Total = 801.15m
                    },
                    new Order()
                    {
                        OrderNumber = 2,
                        CustomerName = "Jerry",
                        Area = 20,
                        DateTime = DateTime.Parse("11/21/2000"),
                        Product = new Product() {CostPerSf = 21, LaborCostPerSf = 16, Type = "stone"},
                        State = new State() {StateName = "Ohio", Abbreviation = "OH", TaxRate = 0.08m},
                        LaborCost = 315,
                        MaterialCost = 425,
                        TaxCost = 68.15m,
                        Total = 803.15m
                    }
                });

            }
        }

        public List<Order> GetAllOrderPerDate(DateTime date)
        {
            return TestFiles[$"Orders_{date.Month}{date.Day}{date.Year}.txt"];
        }

        public Order GetOrder(int orderNumber, List<Order> orders )
        {
            var result = orders.Where(o => o.OrderNumber == orderNumber).Select(s=>s);
            var test = result.ToList()[0];
            return test;
        }

        public void AddOrder(Order order)
        {
            var orders = GetAllOrderPerDate(order.DateTime);

            if (orders.Count > 1)
            {
                int i = orders[orders.Count - 1].OrderNumber;
                order.OrderNumber = i + 1;
            }
            else
            {
                order.OrderNumber = 1;
            }
            UpdateDate(order);
        }

        public void UpdateDate(Order order)
        {
            DateTime date = order.DateTime;
            string _fileName = $"Orders_{date.Month}{date.Day}{date.Year}.txt";
            var orders = GetAllOrderPerDate(date);
            orders.Add(order);
            TestFiles[_fileName] = orders;
        }

        public Response<List<Order>> CheckDate(DateTime date)
        {
            string orderName = "Orders_" + $"{date.Month}{date.Day}{date.Year}.txt";
            Response<List<Order>> response = new Response<List<Order>>();
            var result = TestFiles.FirstOrDefault(l => l.Key == orderName);
            if (result.Key != null)
            {
                response.Success = true;
                response.Data = result.Value;
            }
            else
            {
                response.Success = false;
                response.Message = "Could not find date";
            }
            return response;
        }

        public Response<DateTime> CreateDate(DateTime date)
        {
            TestFiles.Add($"Orders_{date.Month}{date.Day}{date.Year}.txt", new List<Order>());
            Response<DateTime> response = new Response<DateTime>();
            response.Data = date;
            response.Success = true;
            return response;
        }

        public void RemoveOrder(Order order)
        {
            DateTime date = order.DateTime;
            string _fileName = $"Orders_{date.Month}{date.Day}{date.Year}.txt";
            var orders = GetAllOrderPerDate(order.DateTime);
            var results = orders.Where(o => o.OrderNumber != order.OrderNumber).Select(s => s);
            TestFiles[_fileName] = orders.ToList();
        }
    }
}
