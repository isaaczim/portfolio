﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.Data.Repo_Interfaces;
using FlooringApplication.Models;

namespace FlooringApplication.Data
{
    public class ProductRepository : IProductRepository
    {
        private static List<Product> AllProducts;
        private const string _FilePath = @"DataFiles/Products.txt";

        public ProductRepository()
        {
            GetAllProducts();
        }

        public List<Product> GetAllProducts()
        {
            List<Product> results = new List<Product>();
            var rows = File.ReadAllLines(_FilePath);

            for (int i = 1; i < rows.Length; i++)
            {
                var columns = rows[i].Split(',');
                var product = new Product();
                product.Type = columns[0];
                product.CostPerSf = decimal.Parse(columns[1]);
                product.LaborCostPerSf = decimal.Parse(columns[2]);
                results.Add(product);
            }
            return results;
        }
    }
}
