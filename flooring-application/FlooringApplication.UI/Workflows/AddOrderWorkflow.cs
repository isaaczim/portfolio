﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;
using FlooringApplication.Models;
using FlooringApplication.UI.Utilities;

namespace FlooringApplication.UI.Workflows
{
    public class AddOrderWorkflow : IWorkflow
    {
        public void Execute()
        {
            Order newOrder = new Order();
            Response<Product> ProductResponse = new Response<Product>();
            Response<DateTime> DateResponse = new Response<DateTime>();
            Response<State> StateResponse = new Response<State>();

            Console.Clear();
            newOrder.CustomerName = UserPrompts.GetStringFromUser("Please enter your name: ");
            Console.Clear();
            var areaResponse = UserPrompts.GetDecimalFromUser("Please enter the square footage: ");
            if (areaResponse.Success && areaResponse.Data > 0)
            {
                newOrder.Area = areaResponse.Data;
            }
            else
            {
                Console.WriteLine(areaResponse.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }

            Console.Clear();
            ProductResponse = UserPrompts.GetProductFromUser("Please enter the product type\n" +
                                                             "Available product types are Carpet, Tile, Laminate and Wood: ");
            if (ProductResponse.Success)
                newOrder.Product = ProductResponse.Data;
            else
            {
                Console.WriteLine(ProductResponse.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }

            Console.Clear();
            StateResponse = UserPrompts.GetStateFromUser("Please enter the State abbreviation\n" +
                                                         "Available states are Ohio(OH), Pennsylvania(PA), Michigan(MI) and Indiana(IN): ");
            if (StateResponse.Success)
                newOrder.State = StateResponse.Data;
            else
            {
                Console.WriteLine(StateResponse.Message);
                UserPrompts.PressKeyForContinue();
                return;
            }

            Console.Clear();
            DateResponse = UserPrompts.GetDateFromUser("Please enter the date");
            OrderManager manager = new OrderManager();
            if (DateResponse.Success)
                newOrder.DateTime = DateResponse.Data;
            else
            {
                if (DateResponse.Message == "Date does not exist.")
                {
                    manager.CreateDate(DateResponse.Data);
                    newOrder.DateTime = DateResponse.Data;
                }
                else
                {
                    Console.WriteLine(DateResponse.Message);
                    UserPrompts.PressKeyForContinue();
                    return;
                }
            }
            
            newOrder = manager.CalculateOrder(newOrder);
            Screens.DisplayOrder(newOrder);
            char input = UserPrompts.GetAnswerFromUser("Would you like to keep the Order? (Y/N): ");
            if (input == 'Y')
            {
                manager.AddOrder(newOrder);
                Console.WriteLine("Order Successfully Added");
                UserPrompts.PressKeyForContinue();
            }
            else
            {
                Console.WriteLine("Order Cancelled.");
                UserPrompts.PressKeyForContinue();
            }
        }
    }
}
