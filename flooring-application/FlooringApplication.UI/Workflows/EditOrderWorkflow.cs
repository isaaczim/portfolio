﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlooringApplication.BLL;
using FlooringApplication.Models;
using FlooringApplication.UI.Utilities;

namespace FlooringApplication.UI.Workflows
{
    public class EditOrderWorkflow : IWorkflow
    {
        public void Execute()
        {
            Order newOrder = new Order();
            OrderManager manager = new OrderManager();

            Console.Clear();
            var dateResponse = UserPrompts.GetDateFromUser("Please enter the date of order you would like to edit");
            if (!dateResponse.Success)
            {
                Console.WriteLine(dateResponse.Message);
            }
            else
            {
                var orders = manager.GetAllOrders(dateResponse.Data);
                foreach (var order in orders)
                {
                    Screens.DisplayOrder(order);
                }
                var n =
                    UserPrompts.GetIntFromUser("\nPlease enter the order number of the order you would like to edit: ");
                var orderResponse = manager.GetOrder(dateResponse.Data, n);
                if (orderResponse.Success)
                {
                    Console.Clear();
                    Screens.DisplayOrder(orderResponse.Data);
                    string newName = UserPrompts.GetStringFromUser(
                        $"\nEnter new name, or nothing to keep original ({orderResponse.Data.CustomerName}): ");
                    if (string.IsNullOrEmpty(newName))
                        newOrder.CustomerName = orderResponse.Data.CustomerName;
                    else
                        newOrder.CustomerName = newName;

                    var newType = UserPrompts.GetProductFromUser(
                        $"\nEnter new product type, or nothing to keep original ({orderResponse.Data.Product.Type})\n" +
                        $"Available product types are Carpet, Tile, Laminate and Wood: ");
                    if (newType.Success)
                        newOrder.Product = newType.Data;
                    else
                        newOrder.Product = orderResponse.Data.Product;

                    var newState = UserPrompts.GetStateFromUser(
                        $"\nEnter new state or nothing to keep original ({orderResponse.Data.State.StateName})\n" +
                        $"Available states are Ohio(OH), Pennsylvania(PA), Michigan(MI) and Indiana(IN)");
                    if (newState.Success)
                        newOrder.State = newState.Data;
                    else
                        newOrder.State = orderResponse.Data.State;

                    var newDate = UserPrompts.GetDateFromUser(
                        $"\nEnter new date, or nothing to keep original ({orderResponse.Data.DateTime})");
                    if (newDate.Success)
                        newOrder.DateTime = newDate.Data;
                    else
                        newOrder.DateTime = orderResponse.Data.DateTime;

                    var newArea =
                        UserPrompts.GetDecimalFromUser(
                            $"\nEnter the new area, or nothing to keep the original ({orderResponse.Data.Area}): ");
                    if (!newArea.Success)
                        newOrder.Area = orderResponse.Data.Area;
                    else
                        newOrder.Area = newArea.Data;

                    newOrder = manager.CalculateOrder(newOrder);
                    Console.Clear();
                    Screens.DisplayOrder(newOrder);
                    char confirm = UserPrompts.GetAnswerFromUser("Is the displayed order correct?(Y/N): ");
                    if (confirm == 'Y')
                    {
                        manager.AddOrder(newOrder);
                        manager.RemoveOrder(orderResponse.Data);
                        Console.Clear();
                        Console.WriteLine("Order has been updated");
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Original order was kept.");
                    }
                    UserPrompts.PressKeyForContinue();
                }
                else
                {
                    Console.WriteLine(orderResponse.Message);
                }

            }
        }
    }
}
